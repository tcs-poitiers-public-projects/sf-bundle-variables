CHANGELOG
=========
## [0.4.0] - 2023-08-16
### Added
- [#18] Ajoute le type "array"

## [0.3.0] - 2023-05-10
### Added
- [#17] Ajoute la compatibilité avec Symfony 6.x et PHP 7/8

## [0.2.3] - 2020-04-27
### Added
- [#15] Ajoute la compatibilité avec Symfony 5.x

## [0.2.2] - 2020-01-29
### Added
- [#14] Ajoute la compatibilité avec Symfony 4.x

### Changed
- [#13] Mise à jour du readme

## [0.2.1] - 2019-03-19
### Added
- Ajoute la compatibilité avec Symfony 3.x

## [0.2.0] - 2019-01-28
### Added
- [#11] Ajoute un type date

### Fixed
- Corrige la récupération de variables complexe depuis un fichier yml

## [0.1.1] - 2018-12-13
### Added
- [#10] Ajoute un mécanisme permettant de surcharger le bundle

### Fixed
- Corrige une erreur de syntaxe
- [#8] Corrige certains champs pouvant être vides
    
## [0.1.0] - 2017-10-16
### Added
- Initialisation du projet
