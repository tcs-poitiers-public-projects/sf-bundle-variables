<?php

namespace TCS\VariableBundle\Form\Type;

use TCS\VariableBundle\Services\ManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type as Types;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use TCS\VariableBundle\Services\Normalizer;

/** Deprecate this class as projects have their own tools to create formTypes */
class ConfigType extends AbstractType
{

    protected ManagerInterface $manager;

    /**
     * @param ManagerInterface $manager
     */
    public function __construct(ManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        foreach ($options['variables'] as $variableName) {
            $variableOptions = $this->manager->getOptions($variableName);

            $formName = $variableName;
            $formOptions = [
                'required'   => $variableOptions['required'],
                'label'      => $variableName,
            ];

            switch ($variableOptions['type']) {
                case Normalizer::TYPE_INTEGER:
                    $formType = Types\IntegerType::class;
                    break;

                case Normalizer::TYPE_NUMBER:
                    $formType = Types\NumberType::class;
                    break;

                case Normalizer::TYPE_STRING:
                default:
                    $formType = Types\TextType::class;
                    break;

                case Normalizer::TYPE_TEXT:
                    $formType = Types\TextareaType::class;
                    break;

                case Normalizer::TYPE_URL:
                    $formType = Types\UrlType::class;
                    break;

                case Normalizer::TYPE_EMAIL:
                    $formType = Types\EmailType::class;
                    break;

                case Normalizer::TYPE_BOOLEAN:
                    $formType = Types\CheckboxType::class;
                    $formOptions['required'] = false;
                    break;

                case Normalizer::TYPE_ENUM:
                    $formType = Types\ChoiceType::class;
                    $formOptions['multiple'] = false;
                    $formOptions['choices_as_values'] = true;
                    $formOptions['choices'] = array_combine(
                        array_map(
                            function ($choice) use ($formName) {
                                return $formName.'.'.$choice;
                            },
                            $variableOptions['values']
                        ),
                        $variableOptions['values']
                    );
                    break;

                case Normalizer::TYPE_DATETIME:
                    $formType = Types\DateTimeType::class;
                    $formOptions['widget'] = 'single_text';
                    break;

                case Normalizer::TYPE_DATE:
                    $formType = Types\DateType::class;
                    $formOptions['widget'] = 'single_text';
                    break;
            }

            $builder->add(
                $formName,
                $formType,
                $formOptions
            );
        }

    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'tcsvariable_config';
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $names = $this->manager->getNames();

        $resolver->setDefaults(
            [
                'variables' => $names
            ]
        );

        $resolver->setAllowedTypes('variables', ['null', 'array']);
        $resolver->setAllowedValues(
            'variables',
            function ($value) use ($names) {
                if (is_array($value)) {
                    if (count(array_diff($value, $names)) > 0) {
                        return false;
                    }
                }

                return true;
            }
        );
    }

}