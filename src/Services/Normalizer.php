<?php

namespace TCS\VariableBundle\Services;

class Normalizer implements NormalizerInterface
{
    const TYPE_INTEGER = 'integer';
    const TYPE_NUMBER = 'number';
    const TYPE_STRING = 'string';
    const TYPE_TEXT = 'text';
    const TYPE_EMAIL = 'email';
    const TYPE_URL = 'url';
    const TYPE_BOOLEAN = 'boolean';
    const TYPE_DATETIME = 'datetime';
    const TYPE_DATE = 'date';
    const TYPE_ENUM = 'enum';
    const TYPE_ARRAY = 'array';

    const DATETIME_FORMAT = 'Y-m-d H:i:s';
    const DATE_FORMAT = 'Y-m-d';

    /**
     * @return array
     */
    public static function getTypes()
    {
        return [
            static::TYPE_INTEGER,
            static::TYPE_NUMBER,
            static::TYPE_STRING,
            static::TYPE_TEXT,
            static::TYPE_EMAIL,
            static::TYPE_URL,
            static::TYPE_BOOLEAN,
            static::TYPE_DATETIME,
            static::TYPE_DATE,
            static::TYPE_ENUM,
            static::TYPE_ARRAY,
        ];
    }

    /**
     * @param mixed  $raw
     * @param string $type
     * @param array  $options
     *
     * @throws \Exception
     * @return string
     */
    public function normalize($raw, string $type, array $options = [])
    {
        if (null === $raw) {
            return null;
        }

        switch ($type) {
            case static::TYPE_INTEGER:
                return sprintf('%d', $raw);

            case static::TYPE_NUMBER:
                return sprintf('%.9F', $raw);

            case static::TYPE_STRING:
            case static::TYPE_EMAIL:
            case static::TYPE_URL:
            case static::TYPE_ENUM:
                return trim(preg_replace('/(\s+)/', ' ',$raw));

            case static::TYPE_TEXT:
                return strval($raw);

            case static::TYPE_BOOLEAN:
                return $raw ? '1' : '0';

            case static::TYPE_DATETIME:
                if (!($raw instanceof \DateTime)) {
                    throw new \Exception(
                        'Value must be an instance of \DateTime when using the "'.static::TYPE_DATETIME.'" type'
                    );
                }

                return $raw->format(static::DATETIME_FORMAT);

            case static::TYPE_DATE:
                if (!($raw instanceof \DateTime)) {
                    throw new \Exception(
                        'Value must be an instance of \DateTime when using the "'.static::TYPE_DATE.'" type'
                    );
                }

                return $raw->format(static::DATE_FORMAT);

            case static::TYPE_ARRAY:
                $values = [];
                $subtype = $options['subtype'] ?: static::TYPE_STRING;
                foreach ($raw as $value) {
                    $values[] = $this->normalize($value, $subtype);
                }
                return json_encode($values);
            default:
                throw new \Exception('Unknown type : "'.$type.'"');
        }
    }

    /**
     * @param        $normalized
     * @param string $type
     * @param array  $options
     *
     * @throws \Exception
     * @return bool|\DateTime|float|int|array
     */
    public function denormalize($normalized, string $type, array $options = [])
    {
        if (null === $normalized) {
            return null;
        }

        switch ($type) {
            case static::TYPE_INTEGER:
                return intval($normalized);

            case static::TYPE_NUMBER:
                return floatval($normalized);

            case static::TYPE_STRING:
            case static::TYPE_TEXT:
            case static::TYPE_EMAIL:
            case static::TYPE_URL:
            case static::TYPE_ENUM:
                return strval($normalized);

            case static::TYPE_BOOLEAN:
                return !!$normalized;

            case static::TYPE_DATETIME:
                return \DateTime::createFromFormat(static::DATETIME_FORMAT, $normalized);

            case static::TYPE_DATE:
                return (\DateTime::createFromFormat(static::DATE_FORMAT, $normalized))->setTime(0,0);

            case static::TYPE_ARRAY:
                $values = [];
                $json = json_decode($normalized);
                $subtype = $options['subtype'] ?: static::TYPE_STRING;

                foreach ($json as $value) {
                    $values[] = $this->denormalize($value, $subtype);
                }
                return $values;
            default:
                throw new \Exception('Unknown type : "'.$type.'"');
        }
    }
}
