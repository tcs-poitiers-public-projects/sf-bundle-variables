<?php

namespace TCS\VariableBundle\Services;

interface ManagerInterface
{
    /**
     * @param array $configuration
     * @return mixed
     */
    public function loadConfiguration(array $configuration);

    /**
     * @param string $name
     * @param $default
     * @return mixed
     */
    public function get(string $name, $default = null);

    /**
     * @param string $name
     * @param mixed $value
     * @return mixed
     */
    public function set(string $name, $value);

    /**
     * @return array
     */
    public function getNames(): array;

    /**
     * @param string $name
     * @return array
     */
    public function getOptions(string $name): array;
}