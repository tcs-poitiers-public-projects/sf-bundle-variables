<?php

namespace TCS\VariableBundle\Services;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NoResultException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use TCS\VariableBundle\Entity\Variable;

/**
 * Class Manager
 */
class Manager implements ManagerInterface
{
    protected EntityManagerInterface $entityManager;

    protected NormalizerInterface $normalizer;

    protected ValidatorInterface $validator;

    protected ?array $configuration;

    protected ?array $values;

    protected array $localValues;

    /**
     * Manager constructor.
     * @param EntityManagerInterface $entityManager
     * @param NormalizerInterface    $normalizer
     * @param ValidatorInterface     $validator
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        NormalizerInterface $normalizer,
        ValidatorInterface $validator
    ) {
        $this->entityManager = $entityManager;
        $this->normalizer = $normalizer;
        $this->validator = $validator;

        $this->configuration = [];
        $this->values = [];
        $this->localValues = [];
    }

    /**
     * @param array<string, array<string, mixed>> $configuration
     * @return mixed|void
     */
    public function loadConfiguration(array $configuration): void
    {
        $this->configuration = $configuration;

        foreach ($configuration as $name => $options) {

            if (empty($options['data'])) {
                continue;
            }

            $value = $this->normalizer->denormalize($options['data'], $options['type'], $options);

            $errors = $this->validate($value, $options);

            if (count($errors) > 0) {
                throw new \DomainException(
                    sprintf(
                        'Default value (%s) provided for variable "%s" does not comply with type : %s',
                        $options['data'],
                        $name,
                        $options['type']
                    )
                );
            } else {
                $this->localValues[$name] = $value;
            }
        }
    }

    /**
     * @return array
     */
    public function getNames(): array
    {
        if (empty($this->configuration)) {
            throw new \RuntimeException('Variable configuration is not loaded');
        }

        return array_keys($this->configuration);
    }

    /**
     * @param string $name
     * @return array<string, mixed>
     */
    public function getOptions(string $name): array
    {
        if (empty($this->configuration)) {
            throw new \RuntimeException('Variable configuration is not loaded');
        }

        if (!isset($this->configuration[$name])) {
            throw new \InvalidArgumentException('Variable named "'.$name.'" does not exist');
        }

        return $this->configuration[$name];
    }


    /**
     * @param string $name
     * @param $default
     * @return mixed|null
     */
    public function get(string $name, $default = null)
    {
        $options = $this->getOptions($name);

        $this->loadValues();

        if (isset($this->values[$name])) {
            return $this->values[$name];
        }

        if (isset($options['data'])) {
            return $options['data'];
        }

        return $default;
    }


    /**
     * @return array|false
     */
    public function all()
    {
        $names = $this->getNames();

        return array_combine(
            $names,
            array_map(
                function ($name) {
                    return $this->get($name);
                },
                $names
            )
        );
    }

    /**
     * @param string $name
     * @param mixed $value
     * @param bool $returnErrors
     * @return ConstraintViolationListInterface|bool
     */
    public function set(string $name, $value, bool $returnErrors = false)
    {
        $options = $this->getOptions($name);

        // validate value
        $errors = $this->validate($value, $options);

        if (count($errors) > 0) {
            if ($returnErrors) {
                return $errors;
            }

            $errorMessages = [];

            foreach ($errors as $error) {
                $errorMessages[] = $error->getMessage();
            }

            throw new \DomainException(
                sprintf(
                    'Invalid value (%s) for variable "%s". %s',
                    $value,
                    $name,
                    implode(', ', $errorMessages)
                )
            );
        }

        // set the value
        $this->syncValue($name, $value);

        return true;
    }

    /**
     * @param mixed $value
     * @param array $options
     * @return ConstraintViolationListInterface
     */
    protected function validate($value, array $options): ConstraintViolationListInterface
    {
        $constraints = [];

        if ($options['required']) {
            $constraints[] = new Assert\NotNull();
        }

        switch ($options['type']) {
            case Normalizer::TYPE_INTEGER:
                $constraints[] = new Assert\Type('int');
                break;

            case Normalizer::TYPE_NUMBER:
                $constraints[] = new Assert\Type('float');
                break;

            case Normalizer::TYPE_EMAIL:
                $constraints[] = new Assert\Email();
                break;

            case Normalizer::TYPE_URL:
                $constraints[] = new Assert\Url();
                break;

            case Normalizer::TYPE_BOOLEAN:
                $constraints[] = new Assert\Type('boolean');
                break;

            case Normalizer::TYPE_DATE:
            case Normalizer::TYPE_DATETIME:
                $constraints[] = new Assert\Type('\DatetimeInterface');
                break;

            case Normalizer::TYPE_ENUM:
                $constraints[] = new Assert\Choice(['choices' => $options['values']]);
                break;

            default:
                break;
        }

        return $this->validator->validate($value, $constraints);
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    protected function syncValue(string $name, $value)
    {
        $repo = $this->entityManager->getRepository(Variable::class);

        try {
            $variable = $repo->find($name);
            if (!$variable) {
                $variable = Variable::create($name);
                $this->entityManager->persist($variable);
            }

            $variable->setValue($this->normalizer->normalize($value, $this->configuration[$name]['type'], $this->configuration[$name]));

            $this->entityManager->flush();

            $this->values[$name] = $value;
        } catch (\Exception $e) {
            throw new \RuntimeException(sprintf('Cannot sync variable (%s) value into DB', $name), 0, $e);
        }
    }

    protected function loadValues()
    {
        if (empty($this->values)) {
            try {
                $variables = $this
                    ->entityManager
                    ->getRepository(Variable::class)
                    ->createQueryBuilder('v')
                    ->getQuery()
                    ->getArrayResult();

                $values = [];
                foreach (array_column($variables, 'value', 'name') as $name => $value) {

                    try {
                        $options = $this->getOptions($name);

                        $values[$name] = $this->normalizer->denormalize($value, $options['type'], $options);
                    } catch (\InvalidArgumentException $e) {
                        continue; // edge-case : obsolete values in DB not defined anymore in configuration
                    }
                }

                $this->values = $values;
            } catch (NoResultException $e) {
                $this->values = [];
            }
        }

        $this->values = array_merge($this->localValues, $this->values);
    }
}
