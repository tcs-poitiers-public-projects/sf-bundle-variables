<?php

namespace TCS\VariableBundle\Services;

interface NormalizerInterface
{
    /**
     * @param mixed $raw
     * @param string $type
     * @return mixed
     */
    public function normalize($raw, string $type, array $options = []);

    /**
     * @param mixed $normalized
     * @param string $type
     * @return mixed
     */
    public function denormalize($normalized, string $type, array $options = []);
}