<?php


namespace TCS\VariableBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TCS\VariableBundle\DependencyInjection\TCSVariableExtension;

class TCSVariableBundle extends Bundle
{

    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
