<?php

namespace TCS\VariableBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\Container;
use TCS\VariableBundle\Services\Normalizer;
use TCS\VariableBundle\Services\NormalizerInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    const VARIABLE_NAME_FORMAT = '^([a-zA-Z0-9_]*)$';

    private Container $container;

    /**
     * @param Container $container
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('tcs_variable');
        $rootNode = null;
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            // BC layer for symfony/config 4.1 and older
            $rootNode = $treeBuilder->root('tcs_variable');
        }

        $rootNode
            ->children()
            ->arrayNode('configuration')
            ->useAttributeAsKey('id')
            ->requiresAtLeastOneElement()
            ->prototype('array')
            ->addDefaultsIfNotSet()
            ->children()
                            // type
                            ->enumNode('type')
                                ->values($this->getTypes())
                                ->isRequired()
                            ->end()

                            // type
                            ->enumNode('subtype')
                            ->values($this->getTypes())
                            ->end()

                            // data
                            ->scalarNode('data')
                            ->info('Default value. Must be defined when "required" is true')
                            ->end()

                            // required
                            ->booleanNode('required')
                                ->defaultFalse()
                            ->end()

                            // values (for enum only)
                            ->arrayNode('values')
                                ->info('Array of allowed values for "enum" variables only')
                                ->defaultValue([])
                                ->prototype('scalar')->end()
                            ->end()
                        ->end()
                    ->end()
                    ->validate()
                        ->always(
                            function ($v) {
                                foreach ($v as $name => $variable) {
                                    if (!preg_match('/'.static::VARIABLE_NAME_FORMAT.'/', $name)) {
                                        throw new InvalidConfigurationException(
                                            sprintf(
                                                'The variable name (%s) is not properly formatted. Allowed : %s',
                                                $name,
                                                static::VARIABLE_NAME_FORMAT
                                            )
                                        );
                                    }

                                    if (!isset($variable['data']) && $variable['required']) {
                                        throw new InvalidConfigurationException(
                                            sprintf(
                                                'Since the variable "%s" is declared as required, you must provide a default value',
                                                $name
                                            )
                                        );
                                    }

                                    if (!($variable['type'] === Normalizer::TYPE_ENUM xor empty($variable['values']))) {
                                        throw new InvalidConfigurationException(
                                            sprintf(
                                                'The allowed values for "%s" must be defined if (and only if) the declared type is "%s"',
                                                $name,
                                                Normalizer::TYPE_ENUM
                                            )
                                        );
                                    }
                                }

                                // validation of default data is performed during Manager service initialization
                                return $v;
                            }
                        )
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }

    /**
     * @return array
     */
    private function getTypes(): array
    {
        $normalizerClass = null;
        if ($this->container->hasParameter('tcs.variable.normalizer.types.class')) {
            $normalizerClass = $this->container->getParameter('tcs.variable.normalizer.types.class');
        }

        $interfaces = [];
        if ($normalizerClass) {
            $interfaces = class_implements($normalizerClass);
        }

        if (in_array(NormalizerInterface::class, $interfaces)) {
            return $normalizerClass::getTypes();
        }

        return Normalizer::getTypes();
    }

}
