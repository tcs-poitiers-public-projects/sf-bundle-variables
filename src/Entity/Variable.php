<?php

namespace TCS\VariableBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

#[ORM\Table('tcs_variable')]
#[ORM\Entity]
class Variable
{

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'name', type: 'string', length: 255)]
    #[ORM\Id]
    private ?string $name = null;

    /**
     * @var string|null
     */
    #[ORM\Column(name: 'value', type: 'text', length: 65535, nullable: true)]
    private ?string $value = null;

    /**
     * @param string $name
     * @return static
     */
    public static function create(string $name): Variable
    {
        $variable = new static();
        $variable->setName($name);

        return $variable;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function __toString(): string
    {
        return $this->getName() ?? '';
    }
}