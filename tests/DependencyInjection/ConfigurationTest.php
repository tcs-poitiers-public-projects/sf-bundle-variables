<?php

namespace TCS\VariableBundle\Tests\DependencyInjection;

use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Container;
use TCS\VariableBundle\DependencyInjection\Configuration;

class ConfigurationTest extends TestCase
{
    /**
     * @param array $config
     * @return array
     */
    protected function processConfig(array $config): array
    {
        $processor = new Processor();
        $configuration = new Configuration($this->createContainerMock());

        return $processor->processConfiguration(
            $configuration,
            [$config]
        );
    }

    /**
     * @return array<array<string, mixed>>
     */
    protected function getDefaultValuesForRequired(): array
    {
        return [
            ['integer', 0],
            ['number', 0.0],
            ['string', ''],
            ['text', ''],
            ['email', ''],
            ['url', ''],
            ['boolean', false],
        ];
    }

    /**
     * @return void
     */
    public function testSimpleVariable(): void
    {
        $config = $this->processConfig(
            [
                'configuration' => [
                    'proxy_url' => [
                        'type' => 'url',
                        'data' => 'proxy.domain.com',
                    ],
                ],
            ]
        );

        $this->assertArrayHasKey('configuration', $config);
        $this->assertArrayHasKey('proxy_url', $config['configuration']);

        $proxyUrlStructure = $config['configuration']['proxy_url'];

        $this->assertArrayHasKey('type', $proxyUrlStructure);
        $this->assertEquals('url', $proxyUrlStructure['type']);

        $this->assertArrayHasKey('required', $proxyUrlStructure);
        $this->assertFalse($proxyUrlStructure['required']);

        $this->assertArrayHasKey('data', $proxyUrlStructure);
        $this->assertEquals('proxy.domain.com', $proxyUrlStructure['data']);
    }

    /**
     * @dataProvider getDefaultValuesForRequired
     *
     * @param string $type
     * @param mixed $value
     * @return void
     */
    public function testRequiredVariableWithData(string $type, $value): void
    {
        $config = $this->processConfig(
            [
                'configuration' => [
                    'test' => [
                        'type'     => $type,
                        'required' => true,
                        'data'     => $value,
                    ],
                ],
            ]
        );

        $this->assertEquals($value, $config['configuration']['test']['data']);
    }

    /**
     * @return void
     */
    public function testRequiredVariableError(): void
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage('Since the variable "proxy_url" is declared as required, you must provide a default value');

        $config = $this->processConfig(
            [
                'configuration' => [
                    'proxy_url' => [
                        'type'     => 'url',
                        'required' => true,
                    ],
                ],
            ]
        );
    }

    /**
     * @return void
     */
    public function testEnumWithoutValuesError(): void
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage('The allowed values for "proxy_url" must be defined if (and only if) the declared type is "enum"');

        $config = $this->processConfig(
            [
                'configuration' => [
                    'proxy_url' => [
                        'type' => 'enum',
                    ],
                ],
            ]
        );
    }

    /**
     * @return void
     */
    public function testValuesWithoutEnumError(): void
    {
        $this->expectException(InvalidConfigurationException::class);
        $this->expectExceptionMessage('The allowed values for "proxy_url" must be defined if (and only if) the declared type is "enum"');

        $config = $this->processConfig(
            [
                'configuration' => [
                    'proxy_url' => [
                        'type'   => 'text',
                        'values' => ['aze', 'rty', 'uio'],
                    ],
                ],
            ]
        );
    }

    /**
     * @return Container
     */
    private function createContainerMock(): Container
    {
        $containerMock = $this->getMockBuilder(Container::class)
            ->disableOriginalConstructor()
            ->getMock();

        $containerMock
            ->expects($this->any())
            ->method('hasParameter')
            ->willReturn(false);

        return $containerMock;
    }


}