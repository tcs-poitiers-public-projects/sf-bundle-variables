<?php

namespace TCS\VariableBundle\Tests\Services;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Exception\Exception;
use TCS\VariableBundle\Services\Normalizer;

class NormalizerTest extends TestCase
{
    /**
     * @return array<array>
     */
    public function provideNormalizationValues(): array
    {
        return [
            // integer
            ['integer', 1, '1'],
            ['integer', -1, '-1'],
            ['integer', 0, '0'],
            ['integer', 'aa', '0'],
            ['integer', 1.54, '1'],
            ['integer', null, null],

            // number
            ['number', 1, '1.000000000'],
            ['number', -1, '-1.000000000'],
            ['number', 0, '0.000000000'],
            ['number', 'aa', '0.000000000'],
            ['number', 1.54, '1.540000000'],
            ['number', null, null],

            // string
            ['string', 'test', 'test'],
            ['string', '  test  test   ', 'test test'],
            ['string', 1, '1'],
            ['string',
                'test
            fdfd',
                'test fdfd',
            ],
            ['string', null, null],

            // text
            ['text', 'test', 'test'],
            ['text', '  test  test   ', '  test  test   '],
            [
                'text',
                'test
            fdfd',
                'test
            fdfd',
            ],
            ['text', 1, '1'],
            ['text', null, null],

            // boolean
            ['boolean', true, '1'],
            ['boolean', false, '0'],
            ['boolean', 1, '1'],
            ['boolean', 0, '0'],
            ['boolean', null, null],

            // datetime
            ['datetime', new \DateTime('2017-09-29 12:00:42'), '2017-09-29 12:00:42'],
            ['datetime', null, null],

            // date
            ['date', new \DateTime('2017-09-29'), '2017-09-29'],
            ['date', null, null],
        ];
    }

    /**
     * @dataProvider provideNormalizationValues
     * @param string $type
     * @param mixed $raw
     * @param mixed $expected
     * @throws \Exception
     */
    public function testNormalize(string $type, $raw, $expected): void
    {
        $normalizer = new Normalizer();
        $result = $normalizer->normalize($raw, $type);

        $this->assertSame($expected, $result);
    }

    /**
     * @return array
     */
    public function provideDenormalizationValues(): array
    {
        return [
            // integer
            ['integer', '1', 1],
            ['integer', '123456', 123456],
            ['integer', '0', 0],
            ['integer', '-1', -1],
            ['integer', null, null],

            // integer
            ['number', '1', 1.0],
            ['number', '123456', 123456.0],
            ['number', '0', 0.0],
            ['number', '-1', -1.0],
            ['number', '12.45', 12.45],
            ['number', '-12.45', -12.45],
            ['number', null, null],

            // boolean
            ['boolean', '1', true],
            ['boolean', '0', false],
            ['boolean', null, null],

            // string
            ['string', 'simple test', 'simple test'],
            ['string', null, null],

            // string
            ['text', 'simple test', 'simple test'],
            ['text', null, null],

            // datetime
            /* date denormalization tests are suject of a specific method */
            ['datetime', null, null],

            ['date', null, null],
        ];
    }

    /**
     * @dataProvider provideDenormalizationValues
     * @param string $type
     * @param mixed $raw
     * @param mixed $expected
     * @throws \Exception
     */
    public function testDenormalize(string $type, $raw, $expected): void
    {
        $normalizer = new Normalizer();
        $result = $normalizer->denormalize($raw, $type);

        $this->assertSame($expected, $result);
    }

    /**
     * @return array
     */
    public function provideDenormalizationDates()
    {
        return [
            // datetime
            ['datetime', '1988-08-15 11:30:00', new \DateTime('1988-08-15 11:30:00')],
            ['date', '1988-08-15', new \DateTime('1988-08-15')],
        ];
    }

    /**
     * @dataProvider provideDenormalizationDates
     * @param string $type
     * @param mixed $raw
     * @param mixed $expected
     * @throws \Exception
     */
    public function testDenormalizeDate(string $type, $raw, $expected): void
    {
        $normalizer = new Normalizer();
        $result = $normalizer->denormalize($raw, $type);

        $this->assertEquals($expected, $result);
    }

    public function testNormalizeUnknownType(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Unknown type : "car"');

        $normalizer = new Normalizer();
        $normalizer->normalize(206, 'car');
    }

    public function testDenormalizeUnknownType(): void
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Unknown type : "car"');

        $normalizer = new Normalizer();
        $normalizer->denormalize(206, 'car');
    }
}