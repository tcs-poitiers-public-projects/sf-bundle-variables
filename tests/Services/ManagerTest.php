<?php

namespace TCS\VariableBundle\Tests\Services;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use TCS\VariableBundle\Tests\MockBuilderTrait;

class ManagerTest extends KernelTestCase
{
    use MockBuilderTrait;

    public function testGetDefault(): void
    {
        $manager = $this->buildManager();
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'integer',
                    'required' => true,
                    'data'     => 125,
                ],
                'b' => [
                    'type' => 'integer',
                ],
            ]
        );

        $this->assertEquals(125, $manager->get('a'));
        $this->assertNull($manager->get('b'));
        $this->assertEquals(125, $manager->get('b', 125));
    }

    public function testGetValues(): void
    {
        $manager = $this->buildManager(
            [
                ['name' => 'a', 'value' => '124'],
                ['name' => 'b', 'value' => '48.0002'],
                ['name' => 'c', 'value' => 'topinambour'],
                ['name' => 'd', 'value' => '1'],
                ['name' => 'e', 'value' => 'unused'] // should not throw exception
            ]
        );

        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'integer',
                    'required' => true,
                    'data'     => 125,
                ],
                'b' => [
                    'type' => 'number',
                ],
                'c' => [
                    'type' => 'string',
                ],
                'd' => [
                    'type' => 'boolean',
                ],
            ]
        );

        $this->assertEquals(124, $manager->get('a'));
        $this->assertEquals(48.0002, $manager->get('b'));
        $this->assertEquals('topinambour', $manager->get('c'));
        $this->assertTrue($manager->get('d'));
    }

    /**
     * @throws \Exception
     */
    public function testSet(): void
    {
        $manager = $this->buildManager(
            [
                ['name' => 'a', 'value' => '124'],
            ]
        );
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'integer',
                    'required' => true,
                    'data'     => 125,
                ],
                'b' => [
                    'type'     => 'email',
                    'required' => false,
                ],
                'c' => [
                    'type'     => 'url',
                    'required' => false,
                ],
                'd' => [
                    'type'     => 'datetime',
                    'required' => false,
                ],
                'e' => [
                    'type'     => 'date',
                    'required' => false,
                ],
            ]
        );

        $manager->set('a', 126);
        $this->assertSame(126, $manager->get('a'));

        $manager->set('b', 'test@toto.fr');
        $this->assertSame('test@toto.fr', $manager->get('b'));

        $manager->set('c', 'http://www.google.fr');
        $this->assertSame('http://www.google.fr', $manager->get('c'));

        $manager->set('d', new \DateTime('2015-01-01 12:54:12'));
        $this->assertEquals(new \DateTime('2015-01-01 12:54:12'), $manager->get('d'));

        $manager->set('e', new \DateTime('2015-01-01'));
        $this->assertEquals(new \DateTime('2015-01-01'), $manager->get('e'));

    }

    public function testSetInvalidInt(): void
    {
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('Invalid value (doge) for variable "a". This value should be of type int');

        $manager = $this->buildManager();
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'integer',
                    'required' => true,
                    'data'     => 125,
                ],
            ]
        );

        $manager->set('a', 'doge');
    }

    public function testSetInvalidNumber(): void
    {
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('Invalid value (45) for variable "a". This value should be of type float');

        $manager = $this->buildManager();
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'number',
                    'required' => true,
                    'data'     => 125.0,
                ],
            ]
        );

        $manager->set('a', 45);
    }

    public function testSetInvalidBoolean()
    {
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('Invalid value (azerty) for variable "a". This value should be of type boolean');

        $manager = $this->buildManager();
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'boolean',
                    'required' => false,
                ],
            ]
        );

        $manager->set('a', 'azerty');
    }

    public function testSetInvalidEmail()
    {
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('Invalid value (jenesuispasunemail) for variable "a". This value is not a valid email address.');

        $manager = $this->buildManager();
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'email',
                    'required' => false,
                ],
            ]
        );

        $manager->set('a', 'jenesuispasunemail');
    }

    public function testSetInvalidEnum()
    {
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('Invalid value (d) for variable "a". The value you selected is not a valid choice.');

        $manager = $this->buildManager();
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'enum',
                    'required' => false,
                    'values'   => ['a', 'b', 'c'],
                ],
            ]
        );

        $manager->set('a', 'd');
    }

    public function testSetInvalidDateTime()
    {
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('Invalid value (jenesuispasundatetime) for variable "a". This value should be of type \DatetimeInterface.');

        $manager = $this->buildManager();
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'datetime',
                    'required' => false,
                ],
            ]
        );

        $manager->set('a', 'jenesuispasundatetime');
    }

    public function testSetInvalidDate()
    {
        $this->expectException(\DomainException::class);
        $this->expectExceptionMessage('Invalid value (jenesuispasunedate) for variable "a". This value should be of type \DatetimeInterface.');

        $manager = $this->buildManager();
        $manager->loadConfiguration(
            [
                'a' => [
                    'type'     => 'date',
                    'required' => false,
                ],
            ]
        );

        $manager->set('a', 'jenesuispasunedate');
    }

}