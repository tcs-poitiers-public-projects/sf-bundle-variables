<?php

namespace TCS\VariableBundle\Tests;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Validator\Validation;
use TCS\VariableBundle\Services\Manager;
use TCS\VariableBundle\Services\Normalizer;

trait MockBuilderTrait
{

    /**
     * @param array $values
     * @return Manager
     * @throws \Exception
     */
    protected function buildManager(array $values = []): Manager
    {
        return new Manager(
            $this->buildEntityManager($values),
            new Normalizer(),
            Validation::createValidator()
        );
    }

    /**
     * @param array $values
     * @return EntityManagerInterface
     */
    protected function buildEntityManager(array $values = []): EntityManagerInterface
    {
        $query = $this->createMock(AbstractQuery::class);
        $query->expects($this->any())
            ->method('getArrayResult')
            ->willReturn($values);

        $queryBuilder = $this->createMock(QueryBuilder::class);
        $queryBuilder->expects($this->any())
            ->method('getQuery')
            ->willReturn($query);

        $repository = $this->createMock(EntityRepository::class);
        $repository->expects($this->any())
            ->method('createQueryBuilder')
            ->willReturn($queryBuilder);

        $em = $this->createMock(EntityManager::class);
        $em->expects($this->any())
            ->method('getRepository')
            ->willReturn($repository);

        return $em;
    }
}