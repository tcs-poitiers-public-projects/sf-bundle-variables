VariableBundle
==============

## A propos

Ce bundle permet de définir des variables de configuration globale dans une application, dont la valeur peut ensuite être
modifiée au cours du cycle de vie de l'application en base de données.

## Installation

Ajouter une dépendance dans le fichier `composer.json`

```json
"require": {
    ...
    "tcs/variable-bundle": "^0.2"
}
```

Ajouter le dépôt Gitlab dans la liste des `repositories` du fichier `composer.json`

```json
"repositories": [
    ...
    {
        "type": "vcs",
        "url": "https://gitlab.com/tcs-poitiers-public-projects/sf-bundle-variables.git"
    },
    ...
]
```

Rajouter dans la configuration la directive permettant de télécharger directement la `dist` au lieu de `source`, dans
le cas où l'environnement de développement n'autorise la connexion au port 22.

```json
"config": {
    ...
    "preferred-install": {
        "tcs/*": "dist"
    },
    ...
}
```

Lancer la mise à jour des dépendances

```sh
$ composer update
```

#### Symfony 3
Ajouter le TCSVariableBundle dans le fichier `AppKernel.php`

```php
public function registerBundles()
{
    $bundles = array(
        ...
        new TCS\VariableBundle\TCS\VariableBundle(),
        ...
    );
    ...
}
```

Mettre à jour le schéma de la base de données (via la commande de mise à jour ou une migration)

#### Symfony 4/5/6
Ajouter le TCSVariableBundle dans le fichier `config/bundles.php`

```php
    return [
        ...
        new TCS\VariableBundle\TCS\VariableBundle::class => ['all' => true],
        ...
    ];
    ...
```

Mettre à jour le schéma de la base de données (via la commande de mise à jour ou une migration)

## Configuration

#### Symfony 3
Configuration basique dans le fichier `config.yml`

```yaml
tcs_variable: ~
```

#### Symfony 4/5/6
Configuration basique dans le fichier `config/packages/tcs_variable.yml`

```yaml
tcs_variable: ~
```

Déclarer une nouvelle variable

```yaml
tcs_variable:
    configuration:
        my_variable:
            type: string
            data: foo
```

La valeur par défaut de chaque variable est déclarée via l'option `data`

Le type de chaque variable peut être parmi les suivants :

* **integer**: un nombre entier signé
* **number** : un nombre décimal signé
* **string** : une chaîne de caractère
* **text** : un texte pouvant contenir des retours à la ligne
* **email** : une adresse e-mail
* **url** : une URL
* **boolean** : une valeur booléenne
* **enum** : une liste de choix
* **datetime** : une valeur de type date/heure au format `YYYY-MM-DD HH:MM:SS`
* **date** : une valeur de type date au format `YYYY-MM-DD`
* **array** : une liste de valeur au type défini dans l'option `subtype`

Dans le cas d'une variable de type **enum**, la liste des valeurs possibles se déclare via l'option `values`

```yaml
tcs_variable:
    configuration:
        my_list:
            type: enum
            values: [first, second, third]
```

Déclaration d'une variable de type array

```yaml
tcs_variable:
    configuration:
        my_array:
            type: array
            subtype: date
```

> Note : Si aucun `subtype` n'est renseigné, le type `string` est utilisé par défaut.

Déclaration d'une variable obligatoire

```yaml
tcs_variable:
    configuration:
        app_name:
            type: string
            data: My app
            required: true
```

Dans le cas d'une variable obligatoire, la définition de la valeur par défaut est requise
 
## Usage

Un service `Manager` est disponible pour récupérer/administrer les variables

```php
public function __construct(TCS\VariableBundle\Services\Manager $manager){
    ...
}
```

```php
$manager = $this->get('tcs.variable.manager');
```

Récuperer la valeur d'une variable

```php
$manager->get('my_variable', 125);
```

Dans l'exemple ci-dessus, la valeur récupérée sera 

* La valeur définie en base de données, le cas échéant
* Sinon la valeur par défaut définie dans la configuration, le cas échéant
* Sinon la valeur par défaut passée en deuxième paramètre (ici `125`)

Définir la valeur d'une variable

```php
$manager->set('my_variable', 125);
```

## Formulaire

Un FormType est fourni pour générer un formulaire de modification de l'ensemble des variables déclarées.

```php
$form = $this->createForm(
    'TCS\VariableBundle\Form\Type\ConfigType',
    $manager->all()
);
```

Il est possible de paramètrer le formulaire pour n'y inclure que certaines variables

```php
$form = $this->createForm(
    'TCS\VariableBundle\Form\Type\ConfigType',
    null,
    [
        'variables' => ['db_name', 'db_port', 'db_user']
    ]
);
```

## Tests

1. Installer docker et configurer un fichier `docker-compose.yaml` dans le répertoire parent au bundle, exemple :

```yaml
services:
    php:
        container_name: variables_php
        image: php
        volumes:
            - .:/var/www/variables
        restart: unless-stopped
        working_dir: /var/www/variables
```

2. Installer composer et lancer la commande `composer install` depuis le bundle
3. Assurez-vous d'avoir les fichiers `config/bundles.php` et `src/TestKernel.php` d'installés dans votre projet. Le fichier `config/bundles.php` doit ressembler à ça :

```php
return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class                       => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class                        => ['all' => true],
    TCS\VariableBundle\TCSVariableBundle::class                                 => ['all' => true],
];
```

Et le fichier `src/TestKernel.php` :

```php
<?php

namespace TCS\VariableBundle;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

class TestKernel extends BaseKernel
{
    use MicroKernelTrait;
}
```

4. Vérifier le bon fonctionnement du bundle en lançant les tests :

```shell
APP_ENV=test ./vendor/bin/phpunit --stop-on-failure
```

5. Effectuer les modifications sur le projet en s'assurant que les tests fonctionnent.

> Notes : Les fichiers suivants ne doivent pas être commit sur le projet :

* `bin/console`
* `config/packages/doctrine.yaml`
* `config/packages/tcs_variable.yaml`
* `config/bundles.php`
* `src/TestKernel.php`
